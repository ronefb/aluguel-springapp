package br.gov.sp.fatec.aluguelspringapp.entity;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.aluguelspringapp.controller.View;

@Entity
@Table(name = "usr_usuario")
@PrimaryKeyJoinColumn(name="usr_id")
@AttributeOverride(name = "id", column = @Column(name = "usr_id"))
public class Usuario extends Pessoa{

	@JsonView(View.PessoaCompleto.class)
	@Column(name="usr_senha")
	private String senha;
	
	@JsonView(View.AluguelResumo.class)
	@OneToMany(fetch = FetchType.LAZY, mappedBy="vendedor")
	private Set<Aluguel> aluguel;
	
	@JsonView(View.PessoaResumo.class)
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "uau_usuario_autorizacao",
				joinColumns = {@JoinColumn(name = "usr_id")},
				inverseJoinColumns = {@JoinColumn(name = "aut_id")})
	private Set<Autorizacao> autorizacao;
	
	public Set<Autorizacao> getAutorizacao() {
		return autorizacao;
	}

	public void setAutorizacao(Set<Autorizacao> autorizacao) {
		this.autorizacao = autorizacao;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Set<Aluguel> getAluguel() {
		return aluguel;
	}

	public void setAluguel(Set<Aluguel> aluguel) {
		this.aluguel = aluguel;
	}
	
	
}
