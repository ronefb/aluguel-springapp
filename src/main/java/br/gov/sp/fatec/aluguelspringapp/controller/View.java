package br.gov.sp.fatec.aluguelspringapp.controller;

public class View {
	
	public static class AluguelResumo{}
	
	public static class AluguelCompleto extends AluguelResumo{}
	
	public static class PessoaResumo{}
	
	public static class PessoaCompleto extends PessoaResumo{}
	
	public static class UsuarioResumo{}
	
	public static class UsuarioCompleto extends UsuarioResumo{}
	
	public static class AutorizacaoResumo{}
	
	public static class AutorizacaoCompleto extends AutorizacaoResumo{}
	
	

}
