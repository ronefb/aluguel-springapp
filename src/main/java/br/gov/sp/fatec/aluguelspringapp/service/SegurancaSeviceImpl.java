package br.gov.sp.fatec.aluguelspringapp.service;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.gov.sp.fatec.aluguelspringapp.entity.Autorizacao;
import br.gov.sp.fatec.aluguelspringapp.entity.Usuario;
import br.gov.sp.fatec.aluguelspringapp.exceptions.RegistroNaoEcontradoException;
import br.gov.sp.fatec.aluguelspringapp.repository.AutorizacaoRepository;
import br.gov.sp.fatec.aluguelspringapp.repository.UsuarioRepository;


@Service
public class SegurancaSeviceImpl implements SegurancaService{
	
	@Autowired
	private AutorizacaoRepository autRepo;	
	
	@Autowired
	private UsuarioRepository usuRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	@Transactional
	public Usuario criarUsuario(String nome, String senha, String autorizacao) {
		Autorizacao aut = autRepo.findByNome(autorizacao);
		if(aut == null) {
			aut = new Autorizacao();
			aut.setNome(autorizacao);
			autRepo.save(aut);
		}
		Usuario usuario = new Usuario();
		usuario.setNome(nome);
		usuario.setSenha(passwordEncoder.encode(senha));
		usuario.setAutorizacao(new HashSet<Autorizacao>());
		usuario.getAutorizacao().add(aut);
		usuRepo.save(usuario);
		return usuario;
	}
	
	@Override
	@PreAuthorize("hasRole('ADMIN')")
	
	public List<Usuario> buscarTodosUsuarios() {
		
		return usuRepo.findAll();
	}

	@Override
	@PreAuthorize("hasAnyRole('VENDEDOR', 'ADMIN')")
	public Usuario buscarUsuarioPorID(Long id) {
		
		java.util.Optional<Usuario> usuario = usuRepo.findById(id);
		if(usuario.isPresent()) {
			return usuario.get();
		}
		throw new RegistroNaoEcontradoException("Usuário não encontrado");
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public Usuario buscarUsuarioPorNome(String nome) {
		Usuario usuario = usuRepo.findByNome(nome);
		if(usuario != null) {
			return usuario;
		}
		throw new RegistroNaoEcontradoException("Usuário não encontrado");
	}

	@Override
	@PreAuthorize("isAuthenticated()")
	public Autorizacao burcaAutorizacaoPorNome(String nome) {
		Autorizacao autorizacao = autRepo.findByNome(nome);
		if(autorizacao != null) {
			return autorizacao;
		}
		throw new RegistroNaoEcontradoException("Autorização não encontrada");
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuRepo.findByNome(username);
		if(usuario == null) {
			throw new UsernameNotFoundException("Usuario "+username+" encontrado");
		}
		return User.builder().username(username)
				.password(usuario.getSenha())
				.authorities(usuario.getAutorizacao()
				.stream()
				.map(Autorizacao::getNome)
				.collect(Collectors.toList())
				.toArray(new String[usuario
				.getAutorizacao()
				.size()]))
				.build();
	}

}	
