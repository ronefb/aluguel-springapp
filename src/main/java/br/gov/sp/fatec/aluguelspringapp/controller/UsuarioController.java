package br.gov.sp.fatec.aluguelspringapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.annotation.JsonView;

import br.gov.sp.fatec.aluguelspringapp.entity.Autorizacao;
import br.gov.sp.fatec.aluguelspringapp.entity.Usuario;
import br.gov.sp.fatec.aluguelspringapp.service.SegurancaService;

@RestController
@CrossOrigin
@RequestMapping(value = "/usuario")
public class UsuarioController {
	
	@Autowired
	private SegurancaService segService;
	
	@JsonView(View.PessoaResumo.class)
	@GetMapping
	public List<Usuario> buscarTodos(){
		return segService.buscarTodosUsuarios();	
	}
	
	@JsonView(View.PessoaResumo.class)
	@GetMapping(value = "/{id}")
	public Usuario buscarPorId(@PathVariable("id") Long id){
		return segService.buscarUsuarioPorID(id);
	}
	
	@JsonView(View.PessoaResumo.class)
	@GetMapping(value = "/nome")
	public Usuario buscaPorNome(@RequestParam(value="nome") String nome) {
		return segService.buscarUsuarioPorNome(nome);
	}
	
	@JsonView(View.PessoaResumo.class)
	@PostMapping("/add")
	public ResponseEntity<Usuario> cadastraUsuario(@RequestBody Usuario usuario, UriComponentsBuilder uriComponentsBuilder) {
		
		usuario = segService.criarUsuario(usuario.getNome(), usuario.getSenha(), "ROLE_VENDEDOR");
		
		HttpHeaders responseHeaders = new HttpHeaders();
		
		responseHeaders.setLocation(uriComponentsBuilder.path(
				"/usuario/" + usuario.getId()).build().toUri());
		
		return new ResponseEntity<Usuario>(usuario, responseHeaders, HttpStatus.CREATED);
	}
	
	@JsonView(View.AutorizacaoResumo.class)
	@GetMapping(value = "/autorizacao/{autorizacao}")
	public Autorizacao buscaAutorizacaoPorNome(@PathVariable("autorizacao") String nome) {
		return segService.burcaAutorizacaoPorNome(nome);
	}
}
